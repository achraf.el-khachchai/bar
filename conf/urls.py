from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include

from rest_framework.permissions import AllowAny
from drf_yasg import openapi
from drf_yasg.views import get_schema_view


schema_view = get_schema_view(
    openapi.Info(
        title="Test API",
        default_version='v1',
        contact=openapi.Contact(email="el.achraf150@gmail.com"),
    ),
    public=True,
    permission_classes=(AllowAny,)
)


def redirect_root(request):
    return redirect('schema-redoc', permanent=True)


urlpatterns = [
    path('', redirect_root),
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls')),
    path('api/', include('apps.bars.urls')),
    path('api/', include('apps.orders.urls')),
    path('swagger/', schema_view.with_ui(
        'swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui(
        'redoc', cache_timeout=0), name='schema-redoc'),
]
