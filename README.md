# Serveurcom Telecom Test

## Installing

Create a virtual environment at the root of the project.

```shell
python3 -m venv venv
```

Start the virtual environment.

```shell
source venv/bin/activate
```

Update pip.

```shell
(venv) pip install -U pip
```

Install the project's requirements.

```shell
(venv) pip install -r requirements.txt
```

Apply the project's migrations.

```shell
(venv) python manage.py migrate
```

Run the developpement server.

```shell
(venv) python manage.py runserver
```

## Testing

Runs the test suite.

```shell
(venv) python manage.py test
```

## Bootstraping

Loads data fixtures in the database.

```shell
./bootstrap.sh
```

## Documentation

To consult the api documentation visit **/redoc/** or **/swagger/** endpoints.
