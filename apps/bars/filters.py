from django_filters import rest_framework as filters

from apps.bars.models import Reference, Bar, Stock


class ReferenceFilter(filters.FilterSet):
    ref = filters.CharFilter(field_name='ref', lookup_expr='icontains')
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Reference
        fields = ['ref', 'name']


class BarFilter(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Bar
        fields = ['name']


class StockFilter(filters.FilterSet):
    ref = filters.CharFilter(field_name='ref__ref', lookup_expr='icontains')
    name = filters.CharFilter(field_name='ref__name', lookup_expr='icontains')

    class Meta:
        model = Stock
        fields = ['ref', 'name', 'stock']


class MenuFilter(StockFilter):
    stock_only = filters.BooleanFilter(method='filter_stock_only')

    def filter_stock_only(self, queryset, name, value):
        return queryset.exclude(stock=0)

    class Meta:
        model = Stock
        fields = ['ref', 'name', 'bar', 'stock_only']
