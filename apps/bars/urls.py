from django.urls import path, include

from rest_framework.routers import SimpleRouter

from apps.bars.views import (
    ReferenceViewSet, BarViewSet, StockListView, MenuListView)


router = SimpleRouter()
router.register(r'references', ReferenceViewSet)
router.register(r'bars', BarViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('stock/<int:bar_pk>/', StockListView.as_view()),
    path('menu/', MenuListView.as_view()),
]
