from django.db.models import Sum

from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from apps.bars.models import Reference, Bar, Stock
from apps.bars.serializers import (
    ReferenceSerializer, BarSerializer, StockSerializer)
from apps.bars.permissions import IsAdminOrReadOnly
from apps.bars.filters import StockFilter, MenuFilter


class ReferenceViewSet(ModelViewSet):
    """
    References of all beers.
    As a User it is possible to list and retrieve references.
    As a Staff it is possible to create, update and delete references.

    list:
    Return a list of all existing references.

    create:
    Create a new reference.

    retrieve:
    Retrieve the given reference.

    update:
    Update the given reference.

    partial_update:
    Update given fields of a reference.

    delete:
    Delete the given reference.
    """
    queryset = Reference.objects.all()
    serializer_class = ReferenceSerializer
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated, IsAdminOrReadOnly]
    lookup_field = 'ref'
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['ref', 'name']
    ordering_fields = ['ref', 'name']
    ordering = ['pk']


class BarViewSet(ModelViewSet):
    """
    The establishment is split in Bars.
    As a User it is possible to list and retrieve bars.
    As a Staff it is possible to create, update and delete bars.

    list:
    Return a list of all existing bars.

    create:
    Create a new bar.

    retrieve:
    Retrieve the given bar.

    update:
    Update the given bar.

    partial_update:
    Update given fields of a bar.

    delete:
    Delete the given bar.

    ranking:
    Return bars ranked along `all_stocks` `miss_at_least_one` and `most_pints` criterias.
    """
    queryset = Bar.objects.all()
    serializer_class = BarSerializer
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated, IsAdminOrReadOnly]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name']
    ordering_fields = ['name']
    ordering = ['pk']

    @action(detail=False, methods=['get'])
    def ranking(self, request):
        return Response(Bar.objects.ranking_data())


class StockListView(ListAPIView):
    """
    A Stock represents the amount of References in a Bar.
    As a User it is possible to list stocks.

    get:
    Return a list of all stocks for a given bar.
    """
    serializer_class = StockSerializer
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = StockFilter
    ordering_fields = [('ref__ref', 'ref'), ('ref__name', 'name'), 'stock']
    ordering = ['pk']

    def get_queryset(self):
        bar_pk = self.kwargs['bar_pk']
        return Stock.objects.filter(bar=bar_pk)


class MenuListView(ListAPIView):
    """
    Menu of References.
    Anyone can consult the menu.

    get:
    Return a list of References updated with an `availability` field.
    """
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
    pagination_class = PageNumberPagination
    permission_classes = [AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_class = MenuFilter

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        data = queryset.values('ref__ref', 'ref__name', 'ref__description') \
            .order_by('ref_id') \
            .annotate(total=Sum('stock'))
        data = [
            {
                'ref': item['ref__ref'],
                'name': item['ref__name'],
                'description': item['ref__description'],
                'availability': 'outofstock' if item['total'] == 0 else 'available'
            } for item in data
        ]
        return Response(data)
