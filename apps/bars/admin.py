from django.contrib import admin

from apps.bars.models import Reference, Bar, Stock


admin.site.register(Reference)
admin.site.register(Bar)
admin.site.register(Stock)
