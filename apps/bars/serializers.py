from rest_framework import serializers

from apps.bars.models import Reference, Bar, Stock


class ReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reference
        fields = ['ref', 'name', 'description']


class BarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bar
        fields = ['pk', 'name']


class StockSerializer(serializers.ModelSerializer):
    ref = serializers.CharField(source='ref.ref', read_only=True)
    name = serializers.CharField(source='ref.name', read_only=True)
    description = serializers.CharField(
        source='ref.description', read_only=True)

    class Meta:
        model = Stock
        fields = ['ref', 'name', 'description', 'stock']
