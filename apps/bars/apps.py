from django.apps import AppConfig


class BarsConfig(AppConfig):
    name = 'apps.bars'
