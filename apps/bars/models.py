from django.db import models


class Reference(models.Model):
    ref = models.SlugField(unique=True)
    name = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.ref


class BarManager(models.Manager):
    def ranking_data(self):
        """Returns a list of bars ranked by criterias."""
        zero_stocks = Stock.objects.filter(stock=0)
        all_stocks = self.exclude(stocks__in=zero_stocks) \
            .order_by('pk').values_list('pk', flat=True)
        miss_at_least_one = self.exclude(id__in=all_stocks) \
            .order_by('pk').values_list('pk', flat=True)
        instance = self.annotate(total=models.Count('orders__items')) \
            .order_by('-total').first()
        most_pints = [] if instance is None else [instance.pk]
        return [
            {
                "name": "all_stocks",
                "description": "Liste des comptoirs qui ont toutes les références en stock",
                "bars": list(all_stocks)
            },
            {
                "name": "miss_at_least_one",
                "description": "Liste des comptoirs qui ont au moins une référence épuisée",
                "bars": list(miss_at_least_one)
            },
            {
                "name": "most_pints",
                "description": "Liste le comptoir avec le plus de pintes commandées",
                "bars": list(most_pints)
            }
        ]


class Bar(models.Model):
    name = models.CharField(max_length=255)
    refs = models.ManyToManyField(
        Reference, through='Stock', related_name='bars')

    objects = BarManager()

    def __str__(self):
        return self.name


class Stock(models.Model):
    ref = models.ForeignKey(
        'Reference', on_delete=models.CASCADE, related_name='stocks')
    bar = models.ForeignKey(
        Bar, on_delete=models.CASCADE, related_name='stocks')
    stock = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.ref} for {self.bar}'
