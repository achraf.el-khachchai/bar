from django.test import TransactionTestCase

from apps.bars.models import Reference, Bar, Stock
from apps.bars.filters import ReferenceFilter, BarFilter, StockFilter, MenuFilter


class ReferenceFilterTestCase(TransactionTestCase):
    fixtures = ['references.json']

    def test_ref_field(self):
        f = ReferenceFilter({'ref': 'leff'}, queryset=Reference.objects.all())
        self.assertEqual(f.qs.count(), 1)
        self.assertEqual(f.qs[0].pk, 1)

    def test_name_field(self):
        f = ReferenceFilter(
            {'name': 'Brewdog Punk'}, queryset=Reference.objects.all())
        self.assertEqual(f.qs.count(), 1)
        self.assertEqual(f.qs[0].pk, 2)


class BarFilterTestCase(TransactionTestCase):
    fixtures = ['bars.json']

    def test_name_field(self):
        f = BarFilter({'name': 'balcon'}, queryset=Bar.objects.all())
        self.assertEqual(f.qs.count(), 1)
        self.assertEqual(f.qs[0].pk, 4)


class StockFilterTestCase(TransactionTestCase):
    fixtures = ['references.json', 'bars.json', 'stocks.json']

    def test_ref_field(self):
        f = StockFilter({'ref': 'leff'}, queryset=Stock.objects.filter(bar=2))
        self.assertEqual(f.qs.count(), 1)
        self.assertEqual(f.qs[0].ref.pk, 1)

    def test_name_field(self):
        f = StockFilter(
            {'name': 'Brewdog Punk'}, queryset=Stock.objects.filter(bar=2))
        self.assertEqual(f.qs.count(), 1)
        self.assertEqual(f.qs[0].ref.pk, 2)

    def test_stock_field(self):
        f = StockFilter({'stock': 5}, queryset=Stock.objects.filter(bar=2))
        self.assertEqual(f.qs.count(), 2)
        self.assertEqual(f.qs[0].ref.pk, 1)
        self.assertEqual(f.qs[1].ref.pk, 2)


class MenuFilterTestCase(TransactionTestCase):
    fixtures = ['references.json', 'bars.json', 'stocks.json']

    def test_bar_field(self):
        f = MenuFilter({'bar': 1}, queryset=Stock.objects.all())
        self.assertEqual(f.qs.count(), 3)
        self.assertEqual(f.qs[0].ref.pk, 1)
        self.assertEqual(f.qs[1].ref.pk, 2)
        self.assertEqual(f.qs[2].ref.pk, 3)
        self.assertTrue(all(stock.bar_id == 1 for stock in f.qs))

    def test_stock_only(self):
        f = MenuFilter({'stock_only': True}, queryset=Stock.objects.all())
        self.assertEqual(f.qs.count(), 10)
