from rest_framework import status
from rest_framework.test import APITestCase

from apps.bars.models import Reference, Bar, Stock


REFERENCES_URL = '/api/references/'
REFERENCES_DATA = [
    {
        "ref": "leffeblonde",
        "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
    },
    {
        "ref": "brewdogipa",
        "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
    },
    {
        "ref": "fullerindiapale",
        "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. "
    },
]


class ReferenceAnonymousUserTestCase(APITestCase):

    def test_list_forbidden(self):
        response = self.client.get(REFERENCES_URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReferenceAuthUserTestCase(APITestCase):
    fixtures = ['users.json', 'references.json']

    def setUp(self):
        self.client.login(username='user_a', password='user_a_password')

    def test_list_ok(self):
        response = self.client.get(REFERENCES_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], REFERENCES_DATA)

    def test_list_ordering_ok(self):
        response = self.client.get(REFERENCES_URL, {'ordering': 'ref'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], [
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. "
            },
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
            }
        ])

    def test_create_forbidden(self):
        data = {'ref': 'ref', 'name': 'name', 'description': 'desc'}
        response = self.client.post(REFERENCES_URL, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_ok(self):
        response = self.client.get(f'{REFERENCES_URL}leffeblonde/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            "ref": "leffeblonde",
            "name": "Leffe blonde",
            "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
        })


class ReferenceAdminUserTestCase(APITestCase):
    fixtures = ['users.json', 'references.json', 'bars.json', 'stocks.json']

    def setUp(self):
        self.client.login(username='admin_a', password='admin_a_password')

    def test_list_ok(self):
        response = self.client.get(REFERENCES_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], REFERENCES_DATA)

    def test_create_created(self):
        data = {'ref': 'ref', 'name': 'name', 'description': 'desc'}
        response = self.client.post(REFERENCES_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Reference.objects.count(), 4)

    def test_retrieve_ok(self):
        response = self.client.get(f'{REFERENCES_URL}leffeblonde/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            "ref": "leffeblonde",
            "name": "Leffe blonde",
            "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
        })

    def test_update_ok(self):
        data = {'ref': 'update', 'name': 'name', 'description': 'desc'}
        response = self.client.put(f'{REFERENCES_URL}leffeblonde/', data)
        instance = Reference.objects.get(pk=1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(instance.ref, 'update')
        self.assertEqual(instance.name, 'name')
        self.assertEqual(instance.description, 'desc')

    def test_delete_ok(self):
        response = self.client.delete(f'{REFERENCES_URL}leffeblonde/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Reference.objects.count(), 2)
        self.assertEqual(Stock.objects.count(), 8)


BARS_URL = '/api/bars/'
RANKING_URL = '/api/bars/ranking/'
BARS_DATA = [
    {
        "pk": 1,
        "name": "1er étage"
    },
    {
        "pk": 2,
        "name": "2ème étage"
    },
    {
        "pk": 3,
        "name": "la terrasse"
    },
    {
        "pk": 4,
        "name": "le balcon"
    }
]
RANKING_DATA = [
    {
        "name": "all_stocks",
        "description": "Liste des comptoirs qui ont toutes les références en stock",
        "bars": [1, 3]

    },
    {
        "name": "miss_at_least_one",
        "description": "Liste des comptoirs qui ont au moins une référence épuisée",
        "bars": [2, 4]
    },
    {
        "name": "most_pints",
        "description": "Liste le comptoir avec le plus de pintes commandées",
        "bars": [3]
    }
]


class BarAnonymousUserTestCase(APITestCase):

    def test_list_forbidden(self):
        response = self.client.get(BARS_URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_ranking_forbidden(self):
        response = self.client.get(RANKING_URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class BarAuthUserTestCase(APITestCase):
    fixtures = ['users.json', 'references.json',
                'bars.json', 'stocks.json', 'orders.json']

    def setUp(self):
        self.client.login(username='user_a', password='user_a_password')

    def test_list_ok(self):
        response = self.client.get(BARS_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], BARS_DATA)

    def test_ranking_ok(self):
        response = self.client.get(RANKING_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, RANKING_DATA)

    def test_create_forbidden(self):
        data = {'name': 'name'}
        response = self.client.post(BARS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_ok(self):
        response = self.client.get(f'{BARS_URL}1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            "pk": 1,
            "name": "1er étage"
        })


class BarAdminUserTestCase(APITestCase):
    fixtures = ['users.json', 'references.json',
                'bars.json', 'stocks.json', 'orders.json']

    def setUp(self):
        self.client.login(username='admin_a', password='admin_a_password')

    def test_list_ok(self):
        response = self.client.get(BARS_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], BARS_DATA)

    def test_ranking_ok(self):
        response = self.client.get(RANKING_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, RANKING_DATA)

    def test_create_created(self):
        data = {'name': 'name'}
        response = self.client.post(BARS_URL, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Bar.objects.count(), 5)

    def test_retrieve_ok(self):
        response = self.client.get(f'{BARS_URL}1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            "pk": 1,
            "name": "1er étage"
        })

    def test_update_ok(self):
        data = {'name': 'update'}
        response = self.client.put(f'{BARS_URL}1/', data)
        instance = Bar.objects.get(pk=1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(instance.name, 'update')

    def test_delete_ok(self):
        response = self.client.delete(f'{BARS_URL}1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Bar.objects.count(), 3)
        self.assertEqual(Stock.objects.count(), 9)


class StockTestCase(APITestCase):
    fixtures = ['users.json', 'references.json', 'bars.json', 'stocks.json']

    def test_list_forbidden_for_anonymous_user(self):
        response = self.client.get('/api/stock/2/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_ok_for_auth_user(self):
        self.client.login(username='user_a', password='user_a_password')
        response = self.client.get('/api/stock/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                "stock": 5,
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.",
                "stock": 5,
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. ",
                "stock": 0,
            }
        ])


MENU_DATA = [
    {
        "ref": "leffeblonde",
        "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                "availability": "available"
    },
    {
        "ref": "brewdogipa",
        "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.",
                "availability": "available"
    },
    {
        "ref": "fullerindiapale",
        "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. ",
                "availability": "available"
    }
]


class MenuTestCase(APITestCase):
    fixtures = ['users.json', 'references.json', 'bars.json', 'stocks.json']

    def test_ok_with_anonymous_user(self):
        response = self.client.get('/api/menu/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, MENU_DATA)

    def test_ok_with_auth_user(self):
        self.client.login(username='user_a', password='user_a_password')
        response = self.client.get('/api/menu/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, MENU_DATA)

    def test_ok_with_auth_admin(self):
        self.client.login(username='admin_a', password='admin_a_password')
        response = self.client.get('/api/menu/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, MENU_DATA)
