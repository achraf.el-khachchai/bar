from django.test import TestCase

from apps.bars.models import Reference, Bar, Stock
from apps.bars.serializers import ReferenceSerializer, BarSerializer, StockSerializer


class ReferenceTestCase(TestCase):
    fixtures = ['references.json']

    def test_serializer(self):
        serializer = ReferenceSerializer(Reference.objects.get(pk=1))
        self.assertEqual(serializer.data, {
            "ref": "leffeblonde",
            "name": "Leffe blonde",
            "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
        })

    def test_valid_data(self):
        serializer = ReferenceSerializer(
            data={'ref': 'ref', 'name': 'name', 'description': 'desc'})
        self.assertTrue(serializer.is_valid())

    def test_invalid_data_with_empty_ref(self):
        serializer = ReferenceSerializer(
            data={'ref': '', 'name': 'name', 'description': 'desc'})
        self.assertFalse(serializer.is_valid())

    def test_invalid_data_with_empty_name(self):
        serializer = ReferenceSerializer(
            data={'ref': 'ref', 'name': '', 'description': 'desc'})
        self.assertFalse(serializer.is_valid())

    def test_invalid_data_with_empty_description(self):
        serializer = ReferenceSerializer(
            data={'ref': 'ref', 'name': 'name', 'description': ''})
        self.assertFalse(serializer.is_valid())


class BarTestCase(TestCase):
    fixtures = ['bars.json']

    def test_serializer(self):
        serializer = BarSerializer(Bar.objects.get(pk=1))
        self.assertEqual(serializer.data, {
            "pk": 1,
            "name": "1er étage"
        })

    def test_valid_data(self):
        serializer = BarSerializer(data={'name': 'name'})
        self.assertTrue(serializer.is_valid())

    def test_invalid_data_with_empty_name(self):
        serializer = BarSerializer(data={'name': ''})
        self.assertFalse(serializer.is_valid())


class StockTestCase(TestCase):
    fixtures = ['references.json', 'bars.json', 'stocks.json']

    def test_serializer(self):
        serializer = StockSerializer(Stock.objects.get(pk=1))
        self.assertEqual(serializer.data, {
            "ref": "leffeblonde",
            "name": "Leffe blonde",
            "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
            "stock": 10
        })
