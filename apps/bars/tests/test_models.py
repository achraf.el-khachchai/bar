from django.test import TransactionTestCase

from apps.bars.models import Bar


class DatalessBarTestCase(TransactionTestCase):

    def test_ranking_data(self):
        self.assertEqual(Bar.objects.ranking_data(), [
            {
                "name": "all_stocks",
                "description": "Liste des comptoirs qui ont toutes les références en stock",
                "bars": []
            },
            {
                "name": "miss_at_least_one",
                "description": "Liste des comptoirs qui ont au moins une référence épuisée",
                "bars": []
            },
            {
                "name": "most_pints",
                "description": "Liste le comptoir avec le plus de pintes commandées",
                "bars": []
            }
        ])


class BarTestCase(TransactionTestCase):
    fixtures = ['references.json', 'bars.json', 'stocks.json', 'orders.json']

    def test_ranking_data(self):
        self.assertEqual(Bar.objects.ranking_data(), [
            {
                "name": "all_stocks",
                "description": "Liste des comptoirs qui ont toutes les références en stock",
                "bars": [1, 3]
            },
            {
                "name": "miss_at_least_one",
                "description": "Liste des comptoirs qui ont au moins une référence épuisée",
                "bars": [2, 4]
            },
            {
                "name": "most_pints",
                "description": "Liste le comptoir avec le plus de pintes commandées",
                "bars": [3]
            }
        ])
