from django.db import models

from apps.bars.models import Reference, Bar


class Order(models.Model):
    bar = models.ForeignKey(
        Bar, on_delete=models.CASCADE, related_name='orders')

    def __str__(self):
        return f'order {self.pk} for {self.bar}'


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='items')
    ref = models.ForeignKey(
        Reference, on_delete=models.CASCADE, related_name='order_items')

    def __str__(self):
        return f'{self.ref} for {self.order}'
