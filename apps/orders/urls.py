from django.urls import path, include

from rest_framework.routers import SimpleRouter

from apps.orders.views import OrderViewSet, CreateOrderView


router = SimpleRouter()
router.register(r'orders', OrderViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('order/<int:bar_pk>/', CreateOrderView.as_view()),
]
