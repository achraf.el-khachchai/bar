from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from apps.orders.models import Order
from apps.orders.serializers import OrderSerializer
from apps.orders.permissions import IsAnonymous


class OrderViewSet(ReadOnlyModelViewSet):
    """
    Orders passed by clients.
    As User it is possible to list and retrieve orders.

    list:
    Return a list of orders.

    retrieve:
    Retrieve the given order.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]


class CreateOrderView(CreateAPIView):
    """
    Order beers from a Bar.

    post:
    Send references to order them from a bar.
    """
    serializer_class = OrderSerializer
    permission_classes = [IsAnonymous]

    def create(self, request, *args, **kwargs):
        serializer = OrderSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        order = serializer.save(bar_pk=kwargs['bar_pk'])
        return Response(
            OrderSerializer(order).data, status=status.HTTP_201_CREATED)
