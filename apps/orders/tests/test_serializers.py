from unittest.mock import call, patch

from django.test import TestCase
from django.http import Http404

from apps.bars.models import Stock
from apps.orders.models import Order
from apps.orders.serializers import OrderSerializer

ORDER_PAYLOAD = {
    "items": [
                {"ref": "brewdogipa"},
                {"ref": "brewdogipa"},
                {"ref": "fullerindiapale"},
                {"ref": "leffeblonde"}
    ]
}
ORDER_DATA = {
    "pk": 1,
    "items": [
        {
            "ref": "brewdogipa",
            "name": "Brewdog Punk IPA",
                    "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
        },
        {
            "ref": "brewdogipa",
            "name": "Brewdog Punk IPA",
                    "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
        },
        {
            "ref": "fullerindiapale",
            "name": "Fuller's India Pale Ale",
                    "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. "
        },
        {
            "ref": "leffeblonde",
            "name": "Leffe blonde",
                    "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
        }
    ]
}


class OrderTestCase(TestCase):
    fixtures = ['references.json', 'bars.json', 'stocks.json', 'orders.json']

    def test_serializer_data(self):
        serializer = OrderSerializer(Order.objects.get(pk=1))
        self.assertEqual(serializer.data, ORDER_DATA)

    @patch('apps.orders.signals.low_stock.send')
    def test_serializer_save(self, mock):
        serializer = OrderSerializer(data=ORDER_PAYLOAD)
        self.assertTrue(serializer.is_valid())
        order = serializer.save(bar_pk=1)
        self.assertEqual(order.pk, 4)
        self.assertEqual(order.items.filter(ref_id=1).count(), 1)
        self.assertEqual(order.items.filter(ref_id=2).count(), 2)
        self.assertEqual(order.items.filter(ref_id=3).count(), 1)
        self.assertEqual(Stock.objects.get(bar_id=1, ref_id=1).stock, 9)
        self.assertEqual(Stock.objects.get(bar_id=1, ref_id=2).stock, 8)
        self.assertEqual(Stock.objects.get(bar_id=1, ref_id=3).stock, 9)
        mock.assert_not_called()

    @patch('apps.orders.signals.low_stock.send')
    def test_serializer_save_with_low_stock_bar(self, mock):
        serializer = OrderSerializer(data=ORDER_PAYLOAD)
        self.assertTrue(serializer.is_valid())
        order = serializer.save(bar_pk=4)
        self.assertEqual(order.pk, 4)
        self.assertEqual(order.items.filter(ref_id=1).count(), 1)
        self.assertEqual(order.items.filter(ref_id=2).count(), 2)
        self.assertEqual(order.items.filter(ref_id=3).count(), 1)
        self.assertEqual(Stock.objects.get(bar_id=4, ref_id=1).stock, 0)
        self.assertEqual(Stock.objects.get(bar_id=4, ref_id=2).stock, 0)
        self.assertEqual(Stock.objects.get(bar_id=4, ref_id=3).stock, 0)
        mock.assert_has_calls([
            call(sender=Stock, stock_pk=8), call(sender=Stock, stock_pk=8),
            call(sender=Stock, stock_pk=12), call(sender=Stock, stock_pk=4)
        ])

    def test_serializer_save_with_non_existant_bar(self):
        serializer = OrderSerializer(data=ORDER_PAYLOAD)
        self.assertTrue(serializer.is_valid())
        with self.assertRaises(Http404):
            serializer.save(bar_pk=0)

    def test_serializer_save_with_non_existant_refs(self):
        serializer = OrderSerializer(data={
            "items": [
                {"ref": "foo"},
                {"ref": "bar"},
                {"ref": "leffeblonde"},
            ]
        })
        self.assertFalse(serializer.is_valid())
