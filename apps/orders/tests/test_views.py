from rest_framework import status
from rest_framework.test import APITestCase

from apps.orders.models import Order, OrderItem


ORDERS_URL = '/api/orders/'
ORDERS_DATA = [
    {
        "pk": 1,
        "items": [
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. "
            },
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
            }
        ]
    },
    {
        "pk": 2,
        "items": [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique. "
            }
        ]
    },
    {
        "pk": 3,
        "items": [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !"
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons."
            },
        ]
    }
]


class OrderTestCase(APITestCase):
    fixtures = ['users.json', 'references.json', 'bars.json', 'orders.json']

    def test_list_forbidden_for_anonymous_user(self):
        response = self.client.get(ORDERS_URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_forbidden_for_anonymous_user(self):
        response = self.client.get(f'{ORDERS_URL}1/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_ok_for_auth_user(self):
        self.client.login(username='user_a', password='user_a_password')
        response = self.client.get(ORDERS_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, ORDERS_DATA)

    def test_retrieve_ok_for_auth_user(self):
        self.client.login(username='user_a', password='user_a_password')
        response = self.client.get(f'{ORDERS_URL}1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, ORDERS_DATA[0])


CREATE_ORDER_URL = '/api/order/1/'
CREATE_ORDER_PAYLOAD = {
    "items": [
        {
            "ref": "brewdogipa"
        },
        {
            "ref": "brewdogipa"
        },
        {
            "ref": "fullerindiapale"
        },
        {
            "ref": "leffeblonde"
        }
    ]
}


class CreateOrderTestCase(APITestCase):
    fixtures = ['users.json', 'references.json', 'bars.json', 'stocks.json']

    def test_create_forbidden_for_auth_user(self):
        self.client.login(username='user_a', password='user_a_password')
        response = self.client.post(
            CREATE_ORDER_URL, data=CREATE_ORDER_PAYLOAD)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_created_for_anonymous_user(self):
        response = self.client.post(
            CREATE_ORDER_URL, CREATE_ORDER_PAYLOAD)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, ORDERS_DATA[0])
        self.assertEqual(Order.objects.count(), 1)
        self.assertEqual(OrderItem.objects.count(), 4)

    def test_create_not_found_with_non_existant_bar(self):
        response = self.client.post('/api/order/0/', data=CREATE_ORDER_PAYLOAD)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_bad_request_with_invalid_payload(self):
        response = self.client.post(CREATE_ORDER_URL, data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
