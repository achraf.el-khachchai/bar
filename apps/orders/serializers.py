from django.shortcuts import get_object_or_404

from rest_framework import serializers

from apps.bars.models import Reference, Bar
from apps.orders.models import Order, OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    ref = serializers.SlugRelatedField(
        slug_field='ref', queryset=Reference.objects.all())
    name = serializers.CharField(source='ref.name', read_only=True)
    description = serializers.CharField(
        source='ref.description', read_only=True)

    class Meta:
        model = OrderItem
        fields = ['ref', 'name', 'description']


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = ['pk', 'items']

    def create(self, validated_data):
        bar = get_object_or_404(Bar, pk=validated_data['bar_pk'])
        order = Order.objects.create(bar=bar)
        items = OrderItemSerializer(
            data=validated_data['items'], many=True)
        if items.is_valid():
            items.save(order=order)
        return order
