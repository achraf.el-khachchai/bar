from django.db.models.signals import post_init
from django.dispatch import receiver, Signal

from apps.bars.models import Stock
from apps.orders.models import OrderItem


low_stock = Signal(providing_args=['stock_pk'])


@receiver(post_init, sender=OrderItem)
def update_stock(sender, **kwargs):
    instance = kwargs.get('instance')
    try:
        stock = instance.order.bar.stocks.get(ref=instance.ref)
        if stock.stock <= 2:
            low_stock.send(sender=Stock, stock_pk=stock.pk)
        if stock.stock > 0:
            stock.stock = stock.stock - 1
            stock.save()
    except Stock.DoesNotExist:
        pass
